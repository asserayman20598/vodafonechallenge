import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import io.restassured.RestAssured;
import org.testng.annotations.Test;
public class FirstUserTest {
    @Test
    public void testFirstUser() {
    RestAssured.baseURI = "https://jsonplaceholder.typicode.com";                    //Set the base URI

                                                                                    //Define the expected title for the first post
    String firstPostTitle = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit";

    given()
            .when()
            .get("/posts/1")                                                    //Make a GET request to the "/posts/1" endpoint
            .then()
            .statusCode(200)                                                    //Assert that status code of the response is "OK"
            .assertThat()
            .body("title", equalTo(firstPostTitle));                           //Assert that the title of the first post matches the expected title
}
}
