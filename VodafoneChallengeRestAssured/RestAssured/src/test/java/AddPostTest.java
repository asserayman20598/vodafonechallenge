import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class AddPostTest {

    @Test
    public void AddingPostTC() {

        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";        //Set the base URI
                                                                             //Data for the new post
        String title = "New Post Title";
        String body = "This is the body of the new post.";
        int userId = 1000;

        //Set up the request with content type as JSON and provide the data for the new post in the request body
          given()
                .contentType(ContentType.JSON)
                .body("{ \"title\": \"" + title + "\", \"body\": \"" + body + "\", \"userId\": " + userId + " }")

                  //Make a POST request to the "/posts" endpoint to add a new post

                  .when()
                .post("/posts")


                  .then()
                .statusCode(201)                                         //Assert that status code of the response is "Created"
                .log().all()                                               //Log all details of the request and response
                .body("title",equalTo(title));                          //Assert that the title of the new post in the response matches the expected title



    }
}