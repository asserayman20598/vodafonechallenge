import io.restassured.RestAssured;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class DeletePostTest {

    @Test
    public void deletePostTest() {
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";                //Set the base URI

        int postIdToDelete = 50;                                                     //PostID of the post to be deleted

        given()
                .when()
                .delete("/posts/{id}", postIdToDelete)                            //Make a Delete request to the "/posts/{id}" endpoint to delete a post
                .then()
                .statusCode(200)                                                  //Assert that status code of the response is "OK"
                .log().all();                                                       //Log all request and response details

    }
}