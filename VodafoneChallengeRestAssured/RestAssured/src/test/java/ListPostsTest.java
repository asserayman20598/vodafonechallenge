import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.Test;

public class ListPostsTest {

    @Test
    public void testListPosts() {
                given().baseUri("https://jsonplaceholder.typicode.com")     //Set the base URI
               .when().get("posts")                                         //Get method request on the endpoint "posts"
               .then().log().all()                                             //Log all request and response details
               .assertThat().statusCode(200);                               //Assert that status code of the response is "OK"
    }
}
