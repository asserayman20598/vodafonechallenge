Technologies used in automation the task :
-> IntelliJ IDEA
-> Selenium WebDriver (Java)
-> TestNG framework
-> POM design pattern 
-> XML files for managing test suites
-> Emailable reports

Technologies used in APIs' automation task:
-> IntelliJ IDEA
-> Rest Assured library
-> BDD methodology
-> TestNG annotations
