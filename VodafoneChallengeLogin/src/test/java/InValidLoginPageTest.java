import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class InValidLoginPageTest extends TestBase{
    @Test
    public void InvalidLoginTC() throws InterruptedException {
         new HomePage(driver)
                .clickingOnLoginIcon()
                .clickingOnYesLogMeIn()
                .enterMobileNumber("1126413010")
                .enterPassword("Ma_123456")
                .clickingOnLoginButton();
        String expectedUrlAfterLogin = "https://web.vodafone.com.eg/spa/myHome";
        String actualUrlAfterLogin = driver.getCurrentUrl();
        Assert.assertNotEquals(actualUrlAfterLogin, expectedUrlAfterLogin);
    }
}
