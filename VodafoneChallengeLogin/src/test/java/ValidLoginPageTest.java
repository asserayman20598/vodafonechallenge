
import org.testng.Assert;
import org.testng.annotations.Test;



public class ValidLoginPageTest extends TestBase{


    @Test
    public void validTC() throws InterruptedException {
        new HomePage(driver)
                .clickingOnLoginIcon()
                .clickingOnYesLogMeIn()
                .enterMobileNumber("1092829828")
                .enterPassword("Ma_123456")
                .clickingOnLoginButton();
        String expectedUrlAfterLogin = "https://web.vodafone.com.eg/spa/myHome";
        String actualUrlAfterLogin = driver.getCurrentUrl();
        Assert.assertEquals(actualUrlAfterLogin, expectedUrlAfterLogin);
    }


}


