
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogMeInIsDisplayedTest extends TestBase{

    @Test
    public void logMeInIsDisplayed() {


        new HomePage(driver)
                .clickingOnLoginIcon();
        yesLogMeInElement = driver.findElement(yesLogMeIn);
        Assert.assertTrue(yesLogMeInElement.isDisplayed());
    }
}
