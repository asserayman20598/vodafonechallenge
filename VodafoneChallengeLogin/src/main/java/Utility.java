import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.attributeContains;

public class Utility {

    public void clicking (WebDriver driver,By Locator)
    {
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions
                        .or(ExpectedConditions.not(attributeContains(Locator,"class","btn-disabled"))
                                ,ExpectedConditions.elementToBeClickable(Locator)));
        driver.findElement(Locator).click();
    }
    public void enterText(WebDriver driver ,By Locator ,String text)
    {
        new WebDriverWait(driver, Duration.ofSeconds(20)).until(ExpectedConditions.visibilityOfElementLocated(Locator));
        driver.findElement(Locator).sendKeys(text);
    }
}
