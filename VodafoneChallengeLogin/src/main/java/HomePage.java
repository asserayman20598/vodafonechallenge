import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class HomePage extends Utility {
    private WebDriver driver;
    public HomePage(WebDriver driver) {
        this.driver=driver;
    }

//        private By acceptCookies = By.xpath("//button[@id='onetrust-accept-btn-handler']");
        private By loginIcon = By.id("loginIcon");
        private By yesLogMeIn = By.id("InnerloginBtn");


//    public HomePage ClickingOnAcceptCookies()
//    {
//        Utility.clicking(driver,acceptCookies);
//        return new HomePage(driver);
//    }

        public HomePage clickingOnLoginIcon()
        {
            clicking(driver,loginIcon);
            return new HomePage(driver);
        }

        public LoginPage clickingOnYesLogMeIn() throws InterruptedException {
            Thread.sleep(2000);
            clicking(driver,yesLogMeIn);
            return new LoginPage(driver);
        }
}
