import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage extends Utility {
    private WebDriver driver;
    public LoginPage(WebDriver driver) {
        this.driver=driver;
    }

    private By mobileNumberField = By.id("username");
    private By passwordField = By.id("password");
    private By goToMyAccount = By.id("submitBtn");




    public LoginPage enterMobileNumber(String mNumber)
    {
        enterText(driver,mobileNumberField,mNumber);
        return new LoginPage(driver);
    }

    public LoginPage enterPassword(String pass)
    {
        enterText(driver,passwordField,pass);
        return new LoginPage(driver);

    }

    public LoginPage clickingOnLoginButton() throws InterruptedException {
        Thread.sleep(2000);
        clicking(driver,goToMyAccount);
        return new LoginPage(driver);
    }

}
