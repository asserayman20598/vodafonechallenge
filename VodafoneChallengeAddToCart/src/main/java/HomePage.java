import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {
    private WebDriver driver;
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }
    private By acceptCookies = By.xpath("//button[@id='onetrust-accept-btn-handler']");
    private By loginIcon = By.xpath("//*[@id=\"userProfileMenu\"]/button");
    private By mobileNumberField = By.id("username");
    private By passwordField = By.id("password");
    private By goToMyAccount = By.id("submitBtn");
    private By AllButton = By.xpath("(//button[@class='menuTab'])[1]");
    private By PhonesAndAccessories = By.xpath("(//label[@class='tab-label'])[1]");
    private By SmartPhones = By.xpath("(//label[contains(@for,phones)])[4]");
    private By FirstItem = By.xpath("(//div[@class='product-card'])[1]");
    private By GoldColor = By.xpath("(//button[contains(@class,'gold')])[2]");
    private By TwoHundredFiftySixSize = By.xpath("//button[@class='selected']");
    private By AddToCartButton = By.xpath("//button[@class='add-to-cart']");
    private By CartIcon = By.xpath("//button[@class='cart-btn']");
    private By productName = By.xpath("//p[@class='cartProduct-name']");
    private By productPrice = By.xpath("//h5[@class='cartProduct-price']");
    private WebElement productNameElement;
    private WebElement productPriceElement;


    public HomePage ClickingOnAcceptCookies()
    {
        Utility.clicking(driver,acceptCookies);
        return new HomePage(driver);
    }

    public HomePage ClickingOnLoginIcon()
    {
        Utility.clicking(driver,loginIcon);
        return new HomePage(driver);
    }

    public HomePage enterMobileNumber(String mNumber)
    {
        Utility.enterText(driver,mobileNumberField,mNumber);
        return new HomePage(driver);
    }

    public HomePage enterPassword(String pass)
    {
        Utility.enterText(driver,passwordField,pass);
        return new HomePage(driver);
    }

    public HomePage clickingOnLoginButton() throws InterruptedException {
        Thread.sleep(2000);
        Utility.clicking(driver,goToMyAccount);
        return new HomePage(driver);
    }

    public HomePage ClickingOnAllButton() throws InterruptedException {
        Thread.sleep(2000);
        Utility.clicking(driver,AllButton);
        return new HomePage(driver);
    }

    public HomePage ClickingOnPhonesAndAccessories()
    {
        Utility.clicking(driver,PhonesAndAccessories);
        return new HomePage(driver);
    }

    public HomePage ClickOnSmartPhones()
    {
        Utility.clicking(driver,SmartPhones);
        driver.navigate().to("https://eshop.vodafone.com.eg/en/list?categoryUrls=%2Fsmart-phones");
        return new HomePage(driver);
    }

    public HomePage ClickOnFirstItem()
    {
        Utility.clicking(driver,FirstItem);
        return new HomePage(driver);
    }

    public HomePage ClickOnGoldenColor()
    {
        Utility.clicking(driver,GoldColor);
        return new HomePage(driver);
    }

    public HomePage SelectSize()
    {
        Utility.clicking(driver,TwoHundredFiftySixSize);
        return new HomePage(driver);
    }

    public HomePage ClickOnAddToCartButton()
    {
        Utility.clicking(driver,AddToCartButton);
        return new HomePage(driver);
    }

    public HomePage ClickOnCartIcon()
    {
        Utility.clicking(driver,CartIcon);
        return new HomePage(driver);
    }

    public String getProductName()
    {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(productName));
        productNameElement = driver.findElement(productName);
        String product_Name = productNameElement.getText();
        return product_Name;
    }

    public String getProductPrice()
    {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(productPrice));
        productPriceElement = driver.findElement(productPrice);
        String product_Price = productPriceElement.getText();
        return product_Price;
    }






}
