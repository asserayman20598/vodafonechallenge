import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddToCartTest extends TestBase{


    @Test
    public void AddToCart() throws InterruptedException {
        new HomePage(driver)
        .ClickingOnAcceptCookies()
        .ClickingOnLoginIcon()
        .enterMobileNumber("1092829828")
        .enterPassword("Ma_123456")
        .clickingOnLoginButton()
        .ClickingOnAllButton()
        .ClickingOnPhonesAndAccessories()
        .ClickOnSmartPhones()
        .ClickOnFirstItem()
        .ClickOnGoldenColor()
        .SelectSize()
        .ClickOnAddToCartButton()
        .ClickOnCartIcon();

        String actualName = new HomePage(driver).getProductName();
        String expectedName = "Realme Smart Phone 11 4G";
        String actualPrice = new HomePage(driver).getProductPrice();
        String expectedPrice = "9,499 EGP";
        Assert.assertEquals(actualName,expectedName);
        Assert.assertTrue(actualPrice.contains(expectedPrice));

    }
}
